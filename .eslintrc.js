module.exports = {
    root: true,
    env: {
        node: true
    },
    extends: ['plugin:vue/recommended', '@vue/prettier'],
    rules: {
        'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
        'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
        'vue/v-on-style': 'longform',
        'vue/v-bind-style': 'longform',
        'vue/max-attributes-per-line': '4'
    },
    parserOptions: {
        parser: 'babel-eslint'
    }
}
