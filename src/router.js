import Vue from 'vue'
import Router from 'vue-router'
import NProgress from 'nprogress'

Vue.use(Router)

const router = new Router({
    linkActiveClass: 'active',
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'users-list',
            component: () => import('@/views/UsersList.vue')
        },
        {
            path: '/edituser/:id',
            name: 'edituser',
            component: () => import('@/views/EditUser.vue')
        },
        {
            path: '/adduser',
            name: 'adduser',
            component: () => import('@/views/AddUser.vue')
        },
        {
            path: '/phonebrowser',
            name: 'phonebrowser',
            component: () => import('@/views/PhoneBrowser.vue')
        },
        {
            path: '*',
            component: () => import('@/views/Page404.vue')
        }
    ]
})
router.beforeResolve((to, from, next) => {
    if (to.name) {
        NProgress.start()
    }
    next()
})

router.afterEach(() => {
    NProgress.done()
})
export default router
