import Vue from 'vue'
import App from '@/App.vue'
import router from '@/router.js'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'nprogress/nprogress.css'

Vue.config.productionTip = false

new Vue({
    router,
    render: h => h(App)
}).$mount('#app')
